FROM archlinux/base:latest

ADD files/ /

# Update
RUN pacman --noconfirm -Syu

# Install PHP
RUN pacman --noconfirm --needed -S php72 php72-gd php72-intl php72-sodium

RUN ln -s /usr/bin/php72 /usr/bin/php

# Clean up
RUN rm -f \
      /var/cache/pacman/pkg/* \
      /var/lib/pacman/sync/* \
      /etc/pacman.d/mirrorlist.pacnew

